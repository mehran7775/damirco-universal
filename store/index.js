// import actions from "./actions"
// import mutations from "./mutations"
// import getters from "./getters"
// import state from "./state"

// export default({
//     actions,
//     mutations,
//     getters,
//     state
// })

import EventService from "@/services/EventService.js"
import Vue from 'vue'
export const state = () => ({
  get_home: null,
  product : null
})

export const getters = {
  product(state) {
    return state.product
  }
}
export const mutations = {

  SET_RESULT_HOME(state, payload) {
    state.get_home = payload
  },
  SET_PRODUCT(state, payload) {

    Vue.set(state,'product',payload)
    // state.product = payload
  }
}


export const actions = {
  async get_home({
    commit
  }) {
    try {
      await EventService.get_home()
        .then(response => {
          commit('SET_RESULT_HOME', response.data.result)
        })
    } catch (e) {
      console.log('e', e)
    }

  },
  async do_register({
    commit
  }, form) {
    try {
      await EventService.do_register(form)
        .then(response => {
          console.log('re', response)
        })
      await this.$auth.loginWith('local', {
        data: form
      })
    } catch (e) {
      console.log('e', e.response)
    }
  },
  async do_login({
    commit
  }, form) {
    try {
      await this.$auth.loginWith('local', {
        data: form,
      })
      console.log(this.$auth)
    } catch (e) {
      console.log('e', e.response)
    }
  },
  async get_product({
    commit
  }, slug) {
    try {
      await EventService.get_product(slug)
        .then(response => {
          // alert('f')
          let product = JSON.parse(response.data)

          // console.log(product[0])
          commit('SET_PRODUCT', product)
        })
    } catch (e) {
      console.log('e', e)
    }
  }

}
