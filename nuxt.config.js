export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'front-end-nuxt',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [{
        charset: 'utf-8'
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1'
      },
      {
        hid: 'description',
        name: 'description',
        content: ''
      }
    ],
    link: [{
      rel: 'icon',
      type: 'image/x-icon',
      href: '/favicon.ico'
    }]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/assets/scss/main.scss'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '~/plugins/easySlider'
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    '@nuxtjs/fontawesome'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/bootstrap
    'bootstrap-vue/nuxt',
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/content
    '@nuxt/content',

    '@nuxtjs/auth'
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    baseURL: 'http://127.0.0.1:8000/api'
  },

  // Content module configuration: https://go.nuxtjs.dev/config-content
  content: {},

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    extractCSS: true,
    // transpile: ['easySlider'],
  },
  loading: {
    color: 'blue',
    height: '2px'
  },
  bootstrapVue: {
    icons: true // Install the IconsPlugin (in addition to BootStrapVue plugin
  },
  styleResources: {
    scss: [
      'assets/scss/_variables.scss'
    ]
  },
  auth: {
    strategies: {
      local: {
        token: {
          property: 'token'
        },
        user: {
          user: 'user',
        },
        endpoints: {
          login: {
            url: '/signin',
            method: 'post',
          },
          // logout: false,
          logout: {
            url: '/logout',
            method: 'post'
          },
          user: {
            url: '/user',
            method: 'get'
          }
        },

      }
    }
  },
  fontawesome: {
    // component:'fa',
    icons:{
      solid:true,
      brands:true,
      // solid:['faEventLop','faLock'],
      // brands:true
    },
    imports: [{
      set: '@fortawesome/free-solid-svg-icons',
      icons: ['faHome']
    }]
  }
}
