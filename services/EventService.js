import axios from 'axios'

const apiClient = axios.create({
  baseURL: `http://127.0.0.1:8000/api`,
  // baseURL:'http://damirco.com',
  withCredentials: false,
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  }
})

export default {
  get_home() {
    return apiClient.get('/')
  },
  do_login(form) {
    return apiClient.post('/signin', form)
  },
  do_register(form) {
    return apiClient.post('/signup', form)
  },
  get_product(slug) {
    return apiClient.get('/products-api/' + slug)
  }

}
